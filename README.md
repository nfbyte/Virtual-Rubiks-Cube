Description
---------
The script (`script.mel`) creates scramble, solve and animation procedures for the scene (`scene.mb`).

Installation
---------
1) Download the repository.
2) Copy the script (`script.mel`) to Maya's scripts directory (e.g. `%USERPROFILE%\Documents\maya\version\scripts`). 
3) Open the scene (`scene.mb`) in Maya.
4) Source the script from the Script Editor (Window -> General Editors -> Script Editor).
